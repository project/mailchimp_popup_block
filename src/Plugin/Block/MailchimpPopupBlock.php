<?php

namespace Drupal\mailchimp_popup_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Creates a Mailchimp Popup Block.
 *
 * @Block(
 *  id = "mailchimp_popup_block",
 *  admin_label = @Translation("Mailchimp Popup Block"),
 * )
 */
class MailchimpPopupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $method = $config['method'];

    // Invoke the selected build method.
    switch ($method) {
      case 'automatic':
        $build = $this->buildAutomatic($config);
        break;

      default:
        $build = $this->buildManual($config);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'method' => 'manual',
    ];
  }

  /**
   * Returns the automatic triggered popup block.
   *
   * @param array $config
   *   Block configuration.
   *
   * @return array
   *   Block render array.
   */
  protected function buildAutomatic(array $config) {
    $build = [
      '#cache' => [
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
      ],
    ];
    $build['#attached']['library'][] = 'mailchimp_popup_block/mailchimp_popup_block';

    // Automatic popup block loads mailchimp without data attributes from
    // the block instance. So we need to attach those settings globally to the
    // javascript settings.
    $build['#attached']['drupalSettings']['mailchimp_popup_block'] = [
      'method' => $config['method'],
      'mailchimp_baseurl' => $config['mailchimp_baseurl'],
      'mailchimp_uuid' => $config['mailchimp_uuid'],
      'mailchimp_lid' => $config['mailchimp_lid'],
      'popup_reappear_offset' => $config['popup_reappear_offset'],
    ];

    return $build;
  }

  /**
   * Returns the manual triggered popup block.
   *
   * @param array $config
   *   Block configuration.
   *
   * @return array
   *   Block render array.
   */
  protected function buildManual(array $config) {
    $build = [
      '#theme' => 'mailchimp_popup',
      '#description' => $config['description'],
      '#button_text' => $config['button_text'],
      '#mailchimp_baseurl' => $config['mailchimp_baseurl'],
      '#mailchimp_uuid' => $config['mailchimp_uuid'],
      '#mailchimp_lid' => $config['mailchimp_lid'],
      '#cache' => [
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
      ],
    ];
    $build['#attached']['drupalSettings']['mailchimp_popup_block']['method'] = $config['method'];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['mailchimp'] = [
      '#type' => 'details',
      '#title' => $this->t('Mailchimp configuration.'),
    ];
    $form['mailchimp']['mailchimp_baseurl'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Mailchimp Base URL'),
      '#description' => $this->t('The mailchimp base URL. Have a look into your mailchimp account popup form generation.'),
      '#default_value' => isset($config['mailchimp_baseurl']) ? $config['mailchimp_baseurl'] : NULL,
    ];
    $form['mailchimp']['mailchimp_uuid'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Mailchimp UUID'),
      '#description' => $this->t('The mailchimp UUID.'),
      '#default_value' => isset($config['mailchimp_uuid']) ? $config['mailchimp_uuid'] : NULL,
    ];
    $form['mailchimp']['mailchimp_lid'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Mailchimp List ID'),
      '#description' => $this->t('The mailchimp list id.'),
      '#default_value' => isset($config['mailchimp_lid']) ? $config['mailchimp_lid'] : NULL,
    ];
    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Popup Method'),
      '#description' => $this->t('How should the Popup be opened.'),
      '#default_value' => isset($config['method']) ? $config['method'] : NULL,
      '#options' => [
        'manual' => $this->t('Trigger manually by click on a button'),
        'automatic' => $this->t('Open automatically on pageload'),
      ],
    ];

    // Automatic popup.
    $form['popup_reappear_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Popup reappear offset.'),
      '#description' => $this->t('After how much time should the popup reappear, after it has been closed by the user (in seconds). Set to <code>0</code> for forcing the popup to open at every request.<br> 1 hour: 3600<br> 1 day: 86400<br> 1 week: 604800<br> 1 month: 2630000</li></ul>'),
      '#default_value' => isset($config['popup_reappear_offset']) ? $config['popup_reappear_offset'] : 2630000,
      '#min' => 0,
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'automatic'],
        ],
      ],
    ];

    // Manual popup trigger.
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Intro text for the newsletter, shown before the button.'),
      '#default_value' => isset($config['description']) ? $config['description'] : NULL,
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'manual'],
        ],
      ],
    ];
    $form['button_text'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Button text'),
      '#description' => $this->t('The text of the popup trigger button.'),
      '#default_value' => isset($config['button_text']) ? $config['button_text'] : NULL,
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'manual'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $this->setConfigurationValue('mailchimp_baseurl', $form_state->getValue([
        'mailchimp',
        'mailchimp_baseurl',
      ]));
      $this->setConfigurationValue('mailchimp_uuid', $form_state->getValue([
        'mailchimp',
        'mailchimp_uuid',
      ]));
      $this->setConfigurationValue('mailchimp_lid', $form_state->getValue([
        'mailchimp',
        'mailchimp_lid',
      ]));
      $this->setConfigurationValue('method', $form_state->getValue('method'));
      $this->setConfigurationValue('description', $form_state->getValue('description'));
      $this->setConfigurationValue('button_text', $form_state->getValue('button_text'));
      $this->setConfigurationValue('popup_reappear_offset', $form_state->getValue('popup_reappear_offset'));
    }
  }

}
