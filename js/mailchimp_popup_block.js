/**
 * @file
 * Mailchimp Popup Block listener.
 */

(function (Drupal, window, document) {
  'use strict';

  var mailchimpPopupLoader = document.createElement('script');
  mailchimpPopupLoader.src = '//downloads.mailchimp.com/js/signup-forms/popup/embed.js';
  mailchimpPopupLoader.setAttribute('id', 'mailchimp-embed-library');
  mailchimpPopupLoader.setAttribute('data-dojo-config', 'usePlainJson: true, isDebug: false');
  document.body.appendChild(mailchimpPopupLoader);

  Drupal.behaviors.mailchimpPopupBlockListener = {
    attach: function (context, settings) {
      var method = settings.mailchimp_popup_block.method;

      // For the automatic method load the mailchimp popup, once the embed
      // script has been successfully loaded.
      if (method === 'automatic') {
        var script = document.querySelector('#mailchimp-embed-library');
        script.addEventListener('load', function () {
          var config = {
            baseUrl: settings.mailchimp_popup_block.mailchimp_baseurl,
            uuid: settings.mailchimp_popup_block.mailchimp_uuid,
            lid: settings.mailchimp_popup_block.mailchimp_lid
          };

          var popup_reappear_offset = settings.mailchimp_popup_block.popup_reappear_offset;
          var cookies_just_deleted = false;

          require(['mojo/signup-forms/Loader'], function (L) {
            L.start(config, function () {
            });

            // If the cookie not-exists, the popup should have been shown,
            // so set our cookies.
            if (document.cookie.indexOf('MCPopupClosed=') === -1) {
              var date_keep_closed = new Date();
              date_keep_closed.setFullYear(new Date().getFullYear() + 1);
              document.cookie = 'MCPopupBlockKeepClosed=yes;path=/;expires=' + date_keep_closed.toUTCString() + ';';

              var date_keep_closed_until = new Date();
              // Javascript dates uses milliseconds, so convert it.
              date_keep_closed_until.setTime(date_keep_closed_until.getTime() + (popup_reappear_offset * 1000));
              document.cookie = 'MCPopupBlockKeepClosedUntil=;path=/;expires=' + date_keep_closed_until.toUTCString() + ';';
            }
            // If the cookie exists, the popup was once closed. So we set
            // a custom cookie, that will trigger the deletion of this one,
            // which lasts for a year.
            else {
              // If the control cookie was not set, set it for one year expiry.
              if (document.cookie.indexOf('MCPopupBlockKeepClosed=') === -1) {
                var date_keep_closed = new Date();
                date_keep_closed.setFullYear(new Date().getFullYear() + 1);
                document.cookie = 'MCPopupBlockKeepClosed=yes;path=/;expires=' + date_keep_closed.toUTCString() + ';';
              }

              // If our control cookie was set, but the expiry cookie is missing.
              // Invalidate all cookies, so that the cookie will be shown again.
              if (document.cookie.indexOf('MCPopupBlockKeepClosed=') !== -1 && document.cookie.indexOf('MCPopupBlockKeepClosedUntil=') === -1) {
                document.cookie = 'MCPopupClosed=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;';
                document.cookie = 'MCPopupBlockKeepClosed=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;';
                document.cookie = 'MCPopupBlockKeepClosedUntil=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;';
                cookies_just_deleted = true;
              }

              // If the cookies just have been deleted, reinitialize value.
              if (cookies_just_deleted) {
                var date_keep_closed = new Date();
                date_keep_closed.setFullYear(new Date().getFullYear() + 1);
                document.cookie = 'MCPopupBlockKeepClosed=yes;path=/;expires=' + date_keep_closed.toUTCString() + ';';
              }

              // If own expiry time cookie was not set. Set it.
              if (cookies_just_deleted || document.cookie.indexOf('MCPopupBlockKeepClosedUntil=') === -1) {
                var date_keep_closed_until = new Date();
                // Javascript dates uses milliseconds, so convert it.
                date_keep_closed_until.setTime(date_keep_closed_until.getTime() + (popup_reappear_offset * 1000));
                document.cookie = 'MCPopupBlockKeepClosedUntil=;path=/;expires=' + date_keep_closed_until.toUTCString() + ';';
                cookies_just_deleted = false;
              }
            }
          });
        });
      }

      var trigger = context.querySelector('.mailchimp-popup-block__trigger');
      if (trigger) {
        trigger.addEventListener('click', function (e) {
          e.preventDefault();

          var config = {
            baseUrl: this.getAttribute('data-mailchimp-popup-block-baseurl'),
            uuid: this.getAttribute('data-mailchimp-popup-block-uuid'),
            lid: this.getAttribute('data-mailchimp-popup-block-lid')
          };

          require(['mojo/signup-forms/Loader'], function (L) {
            L.start(config);
          });

          // Set cookies to force opening the popup.
          document.cookie = 'MCPopupClosed=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;';
          document.cookie = 'MCPopupSubscribed=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;';
        });
      }
    }
  };

})(Drupal, this, this.document);
